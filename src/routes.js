import Home from './components/Home.vue';
import Login from './components/Login.vue';
import ApolloExample from './components/ApolloExample.vue';

const routes = [
    { path: '/', component: Home },
    { path: '/login', component: Login },
    { path: '/register', component: ApolloExample }
];

export default routes;

